class TeacherSerializer < ActiveModel::Serializer
  attributes :title, :account, :count_classrooms
  belongs_to :department
  has_many :licenses

  def count_classrooms
    self.object.classrooms.length
  end
end
