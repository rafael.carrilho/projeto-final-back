class DepartmentSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_area, :campus
  has_one :department_coordinator
end
