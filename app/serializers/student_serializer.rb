class StudentSerializer < ActiveModel::Serializer
  attributes :title, :account
  belongs_to :course
end
