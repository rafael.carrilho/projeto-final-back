class ClassroomSerializer < ActiveModel::Serializer
  attributes :id, :name, :room, :schedule, :vacancies
  has_one :teacher
end
