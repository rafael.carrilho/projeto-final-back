class DepartmentCoordinatorSerializer < ActiveModel::Serializer
  attributes :title, :account
  has_one :department
end
