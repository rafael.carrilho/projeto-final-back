class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_area, :campus, :seasons
  has_one :course_coordinator
  
end
