class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :knowledge_area, :total_workload
  has_one :school_year
end
