class SchoolYearSerializer < ActiveModel::Serializer
  attributes :id, :status, :year, :semester
end
