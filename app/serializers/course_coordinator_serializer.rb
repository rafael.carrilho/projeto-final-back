class CourseCoordinatorSerializer < ActiveModel::Serializer
  attributes :title, :account
  has_one :course
end
