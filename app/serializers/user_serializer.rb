class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :cpf, :birthdate, :telephone, :cellphone, :id_number, :nationality, :state, :registration, :role
end
