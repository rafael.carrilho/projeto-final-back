class Classroom < ApplicationRecord
  belongs_to :teacher

  validates :name, :room, :vacancies, :schedule, presence: true
  validates :vacancies, numericality: {greater_than: 20}
  

end
