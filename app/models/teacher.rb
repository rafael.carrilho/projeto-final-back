class Teacher < ApplicationRecord
  belongs_to :user
  belongs_to :department, optional: true
  has_many :licenses
  has_many :classrooms
  
end
