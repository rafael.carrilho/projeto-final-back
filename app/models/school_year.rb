class SchoolYear < ApplicationRecord
    has_many :subjects

    enum status: {plaining:0, inscriptions:1, open:2, closed:3}

    validate :year_cannot_be_less_than_2020, :semester_must_be_1_or_2, :school_year_validate
    def year_cannot_be_less_than_2020
        if year < 2020
            self.errors.add(:year, "Tem que ser maior de 2020")
        end  
    end

    def semester_must_be_1_or_2
        if semester != 1 && semester != 2
            self.errors.add(:semester, "Tem que ser 1 ou 2")
        end
    end

    def school_year_validate
        @school_year = SchoolYear.find_by(year: self.year, semester: self.semester)
        if @school_year
          self.errors.add(:school_year, "Periodo letivo já foi criado")
        end
    end
end
