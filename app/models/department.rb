class Department < ApplicationRecord
    belongs_to :department_coordinator

    validates :name, :knowledge_area, :campus, presence: true
    validates :name, uniqueness: true
end
