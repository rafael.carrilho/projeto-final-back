class Course < ApplicationRecord
  belongs_to :course_coordinator
  has_many :students
  has_many :season_courses
  #has_many :subjects, through: :season_courses

  validates :name, :knowledge_area, :campus, presence: true
  validates :name, uniqueness: true

  def seasons
    arr = []
    self.season_courses.each do |season|
      arr.push(SeasonCourseSerializer.new(season))
    end
    return arr
  end
end
