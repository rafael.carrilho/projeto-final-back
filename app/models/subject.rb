class Subject < ApplicationRecord
  belongs_to :school_year

  validates :name, :knowledge_area, presence: true
  validates :knowledge_area, presence: true
  
end
