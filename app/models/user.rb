class User < ApplicationRecord
    has_secure_password
    has_one :student
    has_one :director
    has_one :teacher
    has_one :department_coordinator
    has_one :course_coordinator

    enum role: {
        director:0,
        department_coordinator: 1,
        course_coordinator: 2,
        teacher: 3,
        student: 4
    }

    validates :name, :email, :cpf, :id_number, :registration, :birthdate, :telephone, :cellphone, :nationality, :state, presence: true
    validates :cpf, :id_number, :registration, :email, uniqueness: true 
    validates :password, :password_confirmation, length: {minimum: 6}, on: :update 
    validates :cpf, format: {with: /\b\d{3}\.\d{3}\.\d{3}-\d{2}\z/, message: "O CPF deve estar no formato 999.999.999-99"}
    validates :id_number, format: {with: /\b\d{2}\.\d{3}\.\d{3}\-\d{1}\z/, message: "O RG deve estar no formato 99.999.999-9"}
    validates :email, format: {with: /\b[A-Z0-9._%a-z\-]+@id\.uff\.br\z/, message: "Utilize o seu email da UFF. Exemplo: meuemail@id.uff.br"}
    before_validation :take_secret_password
    after_create :create_role
    def take_secret_password
        self.password = birthdate.to_s        
    end

    def create_role
        case role
            when "department_coordinator"
                DepartmentCoordinator.create(user_id: id)
            when "course_coordinator"
                CourseCoordinator.create(user_id: id)
            when "teacher"
                Teacher.create(user_id: id)
            when "student"
                Student.create(user_id: id)
            when "director"
                Director.create(user_id: id)
        end
    end

    def view_serializer
        case role
            when "department_coordinator"
                DepartmentCoordinatorSerializer.new(self.department_coordinator)
            when "course_coordinator"
                CourseCoordinatorSerializer.new(self.course_coordinator)
            when "teacher"
                TeacherSerializer.new(self.teacher)
            when "student"
                StudentSerializer.new(self.student)
            when "director"   
                DirectorSerializer.new(self.director)
        end
    end

end
