class SeasonCourse < ApplicationRecord
  belongs_to :course
  # belongs_to :subject

  validates :season, numericality: {greater_than: 0, less_than: 10}
  
end
