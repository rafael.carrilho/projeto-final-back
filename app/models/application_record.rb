class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def account
    UserSerializer.new(self.user)
  end
  def title
    case self.user.role
      when "director"
        "Diretor"
      when "department_coordinator"
        "Coordenador(a) de " << self.department.name
      when "course_coordinator"
        "Coordenador(a) de " << self.course.name
      when "teacher"
        "Professor"
      when "student"
        self.course.name
    end
  end
end
