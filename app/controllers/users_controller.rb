class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  load_and_authorize_resource
  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if current_user.director?
      @user.role = params[:user][:role]
    elsif current_user.department_coordinator?
      @user.role = "teacher"
    elsif current_user.course_coordinator?
      @user.role = "student"
    end
    if @user.save
      if @user.teacher?
        @user.teacher.department_id = current_user.department_coordinator.department.id
        params[:user][:subject_ids]&.each do |id|
          Licenses.create(teacher_id: @user.teacher.id)
        end
        @user.teacher.save
      elsif @user.student?
        @user.student.course_id = current_user.course_coordinator.course.id
        @user.student.save
      end
      
      render json: {user: @user.view_serializer}, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:name, :email, :cpf, :password, :password_confirmation, :birthdate, :telephone, :cellphone, :id_number, :nationality, :state, :registration)
    end
end
