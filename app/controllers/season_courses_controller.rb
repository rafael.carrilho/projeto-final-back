class SeasonCoursesController < ApplicationController
  before_action :set_season_course, only: [:show, :update, :destroy]

  # GET /season_courses
  def index
    @season_courses = SeasonCourse.all

    render json: @season_courses
  end

  # GET /season_courses/1
  def show
    render json: @season_course
  end

  # POST /season_courses
  def create
    @season_course = SeasonCourse.new(season_course_params)

    if @season_course.save
      render json: @season_course, status: :created, location: @season_course
    else
      render json: @season_course.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /season_courses/1
  def update
    if @season_course.update(season_course_params)
      render json: @season_course
    else
      render json: @season_course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /season_courses/1
  def destroy
    @season_course.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_season_course
      @season_course = SeasonCourse.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def season_course_params
      params.require(:season_course).permit(:season, :course_id)
    end
end
