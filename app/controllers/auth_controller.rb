class AuthController < ApplicationController
  def login
    user = User.find_by!(cpf: params[:user][:cpf]) #401
    if user&.authenticate(params[:user][:password])
      token = JsonWebToken.encode(user_id: user.id)
      render json: {token: token, user: user.view_serializer}
    else
      render json: {error: "Senha incorreta"}, status: 403
    end

  end
end
