require 'test_helper'

class SeasonCoursesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @season_course = season_courses(:one)
  end

  test "should get index" do
    get season_courses_url, as: :json
    assert_response :success
  end

  test "should create season_course" do
    assert_difference('SeasonCourse.count') do
      post season_courses_url, params: { season_course: { course_id: @season_course.course_id, season: @season_course.season } }, as: :json
    end

    assert_response 201
  end

  test "should show season_course" do
    get season_course_url(@season_course), as: :json
    assert_response :success
  end

  test "should update season_course" do
    patch season_course_url(@season_course), params: { season_course: { course_id: @season_course.course_id, season: @season_course.season } }, as: :json
    assert_response 200
  end

  test "should destroy season_course" do
    assert_difference('SeasonCourse.count', -1) do
      delete season_course_url(@season_course), as: :json
    end

    assert_response 204
  end
end
