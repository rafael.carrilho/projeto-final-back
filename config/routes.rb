Rails.application.routes.draw do
  resources :classrooms
  resources :season_courses
  resources :courses
  resources :departments
  post 'auth/login'
  get 'users/current', to: "application#validate_user"
  resources :users
  resources :subjects
  resources :school_years
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
