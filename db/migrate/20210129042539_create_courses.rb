class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :knowledge_area
      t.string :campus
      t.references :course_coordinator, foreign_key: true

      t.timestamps
    end
  end
end
