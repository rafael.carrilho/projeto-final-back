class CreateSeasonCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :season_courses do |t|
      t.integer :season
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
