class CreateSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjects do |t|
      t.string :name
      t.string :knowledge_area
      t.integer :total_workload
      t.references :school_year, foreign_key: true

      t.timestamps
    end
  end
end
