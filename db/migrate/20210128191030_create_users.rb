class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :cpf
      t.string :password_digest
      t.date :birthdate
      t.string :telephone
      t.string :cellphone
      t.string :id_number
      t.string :nationality
      t.string :state

      t.timestamps
    end
  end
end
