# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: "Diretor", cpf: "111.222.333-44", id_number: "11.111.111-2", birthdate: "22/01/2000", role: 0, nationality: "Brasil/BR", state: "RJ", telephone: "(21) 3131-3131", cellphone: "(21) 99090-9090", email: "reitor@id.uff.br", registration: "123.031.312-1")
paulo = User.create!(name: "Paulo Cortês", cpf: "222.222.333-44", id_number: "12.111.111-2", birthdate: "22/01/2000", role: 1, nationality: "Brasil/BR", state: "RJ", telephone: "(21) 3131-3232", cellphone: "(21) 99191-9090", email: "gma_coord@id.uff.br", registration: "121.031.312-1")
aline = User.create!(name: "Aline", cpf: "333.222.333-44", id_number: "13.111.111-2", birthdate: "22/01/2000", role: 2, nationality: "Brasil/BR", state: "RJ", telephone: "(21) 3135-3232", cellphone: "(21) 99192-9090", email: "si_coord@id.uff.br", registration: "130.031.312-1")
gma = Department.create!(name:"GMA", knowledge_area:"Matemática", campus:"Praia Vermelha", department_coordinator_id: paulo.department_coordinator.id)

##Criação do curso de SI
si = Course.create!(name:"Sistemas de Informações", knowledge_area:"Tecnologia", campus:"Praia Vermelha", course_coordinator_id: aline.course_coordinator.id)
8.times do |season|
    season_si = SeasonCourse.create(season: season, course_id: si.id)
end

##Teacher
thiago = User.create!(name: "Thiago Andrade", cpf: "444.222.333-44", id_number: "14.111.111-2", birthdate: "22/01/2000", role: 3, nationality: "Brasil/BR", state: "RJ", telephone: "(21) 3135-3232", cellphone: "(21) 99192-9090", email: "thiago_gma@id.uff.br", registration: "130.031.312-4")
thiago.teacher.update(department_id: gma.id)
4.times do |lic|
    license = License.create(teacher_id: thiago.teacher.id)
end

##Student
carrilho = User.create!(name: "Rafael Carrilho", cpf: "555.222.333-44", id_number: "14.111.111-3", birthdate: "22/01/2000", role: 4, nationality: "Brasil/BR", state: "RJ", telephone: "(21) 3135-3232", cellphone: "(21) 99192-9090", email: "carrilho_si@id.uff.br", registration: "130.031.312-5")
carrilho.student.update(course_id: si.id)